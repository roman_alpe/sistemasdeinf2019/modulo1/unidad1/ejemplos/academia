﻿-- Ejemplo de creación de una base de datos de veterinarios
-- Tiene 6 tablas
DROP DATABASE IF EXISTS b20190531;
CREATE DATABASE b20190531;

-- Seleccionar la base de datos
USE b20190531;

/*
  creando la tabla clientes
*/

CREATE TABLE profesor(
  dni int AUTO_INCREMENT,
  nombre varchar(100),
  dir varchar(100),
  tfno int,
  PRIMARY KEY(dni) -- creando la clave
);
CREATE TABLE alumno(
  exp1 int,
  nomb varchar(100),
  apell varchar(100),
  fecha_nac date,
  PRIMARY KEY(exp1) -- creando la clave
  );
CREATE TABLE modulo
  cod int,
  nomb varchar(100),
  );
CREATE TABLE imparte
  dni int,
  cod int,
PRIMARY KEY(dni,cod), -- creando la clave

  -- claves ajenas
CONSTRAINT fkimparteprofes FOREIGN KEY(dni)
REFERENCES profesor(dni),
CONSTRAINT fkimpartemodul FOREIGN KEY(cod)
REFERENCES modulo(cod)
);

CREATE TABLE cursa
  exp int,
  nomb varchar(100),
  fecha_nac date,
PRIMARY KEY(exp1,cod), --creando la clave 
 
 -- claves ajenas
CONSTRAINT fkcursalumno FOREIGN KEY (exp1)
REFERENCES alumno(exp1),
CONSTRAINT fkcursamodulo FOREIGN KEY (cod)
REFERENCES modulo(cod),
);
CREATE TABLE delegado
  exp_deleg int,
  exp_alumn int,
  PRIMARY KEY(exp_deleg,exp_alumn) -- creando la clave
  UNIQUE KEY (exp_alumn),

   -- claves ajenas
  CONSTRAINT fkdeleg_alumn FOREIGN KEY(exp_alum)
  REFERENCES alumno(exp1),
  CONSTRAINT fkrepresenta_del FOREIGN KEY (exp_del)
  REFERENCES alumno(exp1)
);

  






        






